<?php

// Load library
require_once '../vendor/autoload.php';

use odsPhpGenerator\ods;
use odsPhpGenerator\odsTable;
use odsPhpGenerator\odsStyleTableColumn;
use odsPhpGenerator\odsTableColumn;
use odsPhpGenerator\odsTableRow;
use odsPhpGenerator\odsStyleTableRow;
use odsPhpGenerator\odsTableCellString;

// Create Ods object
$ods  = new ods();
$ods->setScaleToX('1');

$table = new odsTable('Column Width');

// create column width 1cm   
$styleColumn = new odsStyleTableColumn();
$styleColumn->setColumnWidth("1cm");
$column1 = new odsTableColumn($styleColumn);

// create column width 7cm
$styleColumn = new odsStyleTableColumn();
$styleColumn->setColumnWidth("7cm");
$column2 = new odsTableColumn($styleColumn);

//create row with 2cm
$styleRow = new odsStyleTableRow();
$styleRow->setRowHeight("20mm");
$styleRow->setUseOptimalRowHeight("false"); //important

// add 2 1cm column
$table->addTableColumn($column1);
$table->addTableColumn($column1);

// add 1 7cm column
$table->addTableColumn($column2);

// Create data
$table->addRow($row = new odsTableRow());
$row->addCell( new odsTableCellString("1cm"));
$row->addCell( new odsTableCellString("1cm"));
$row->addCell( new odsTableCellString("7cm ..."));

$table->addRow($row = new odsTableRow($styleRow));
$row->addCell( new odsTableCellString("2cm x 1cm"));
$row->addCell( new odsTableCellString("2cm x 1cm"));
$row->addCell( new odsTableCellString("2cm x 7cm ..."));

// Add table to ods
$ods->addTable($table);

// Download the file
$ods->downloadOdsFile("ColumnWidth.ods");
  
?>
